module github.com/tosone/golang-macaron

go 1.13

require (
	github.com/elazarl/go-bindata-assetfs v1.0.0 // indirect
	github.com/go-macaron/bindata v0.0.0-20191101042848-1f434a96f7aa // indirect
	github.com/go-macaron/gzip v0.0.0-20191101043656-b5609500c6fc
	github.com/go-macaron/session v0.0.0-20191101041208-c5d57a35f512
	github.com/klauspost/compress v1.9.7 // indirect
	github.com/unknwon/com v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876 // indirect
	gopkg.in/ini.v1 v1.51.1 // indirect
	gopkg.in/macaron.v1 v1.3.4
)
